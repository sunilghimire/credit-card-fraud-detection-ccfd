# Credit Card Fraud Detection CCFD

Card Fraud Detection software is used to assess whether or not a new transaction is fraudulent by modeling past credit card transactions with the information of those that have turned out to be a fraud.